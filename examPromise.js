const firstFunction  = () => {
    return new Promise(
         (resolve, reject) => {
             setTimeout(() => resolve("1"), 3000)
         }
     );
 }
 
 const secondFunction = (r) => {
     return new Promise(
         (resolve, reject) => {
             console.log(r);
             setTimeout(() => resolve("2"), 1000)
         }
     );
 }
 
 const thirdFunction = (r) => {
     return new Promise(
         (resolve, reject) => {
             console.log(r);
             setTimeout(() => resolve("3"), 2000)
         }
     );
 }
 
 // โจทย์ข้อสุดท้ายให้ใช้ promise ในการช่วยให้ console.log ออกมาเรียงลำดับ 1 , 2 , 3 ตามลำดับ
 // โดยไม่อนุญาติให้แก้เลขเวลาใน setTimeout โดยให้ เรียก firstFunction ก่อน หลังจากนั้นจึงตามด้วย
 // secondFunction และ thirdFunction ตามลำดับ
 
 
 firstFunction()
 .then(secondFunction)
 .then(thirdFunction)
 .then(console.log)