

const arrayObject = require('./example-data.json');

let obj = JSON.stringify(arrayObject);
let jsonObject = JSON.parse(obj);

// console.log(jsonObject[1].id);

// ข้อ 1
const product = jsonObject.find(name => name.id === 10207);
console.log('ข้อ 1 -> ', product.name);

//ข้อ 2
console.log('ข้อ 2 -> ผลรวมราคา = ', sumPrice(jsonObject));

// ข้อ3
if(isAllVatEqualZero(jsonObject)) {
    console.log('ข้อ 3 => vat_percent ทั้งหมดเท่ากับ 0');
} else {
    console.log('ข้อ 3 => เช็คแล้วมี vat_percent ไม่เท่ากับ 0');
}

// ข้อ4
// console.log('ข้อ 4 => ')
findProductprice(jsonObject);

// ข้อ5
console.log('ข้อ 5 => ' + isToggleFalse(jsonObject)); 

// ข้อ6
console.log('ข้อ 6 => ' + sumPriceproduct(jsonObject));

// ข้อ 7
console.log('ข้อ 7 => ' + isEditablePrice(jsonObject));

//ข้อ 8
console.log('ข้อ 8 => ' + findFirsttypeNotNull(jsonObject));

//ข้อ 9
console.log(getTypeIdArrayWhichSubProductHasAllType(jsonObject)); 


function sumPrice(jsonObject) {

    const result = jsonObject.reduce((sum,number) => {
        return sum+number.price
      }, 0)
      return result;
}


function isAllVatEqualZero(jsonObject) {

    const everyResult = jsonObject.every((mainProduct) => {
        return mainProduct.vat_percent  === 0
      })
      return everyResult;

    // var count = jsonObject.filter(function(product) { return product.vat_percent != 0; }).length;
    // return count == 0;
}



function findProductprice(jsonObject) {
    jsonObject.forEach((mainProduct) => {
       var countPriceLessthan200 = mainProduct.products.filter((product) => {               
            return product.price  <= 200
        }).length
        if(countPriceLessthan200 > 0) {
            console.log(mainProduct);
        }
     })
}


function isToggleFalse(jsonObject) {
    console.log(jsonObject.map(obj=> ({ ...obj, isToggle: false })));
}

function sumPriceproduct(jsonObject) {
    var array = [];
    jsonObject.forEach((mainProduct) => {
       var productPriceTotal = mainProduct.products.reduce((sum,number) => {
            return sum+number.price
      }, 0)
    //   console.log(productPriceTotal);
      
    array.push({name: mainProduct.name, productsTotalPrice: productPriceTotal});
      
        // if(countPriceLessthan200 > 0) {
        //     console.log(mainProduct);
        // }
     })
     console.log(array);
}

function isEditablePrice (jsonObject) {
    var array = [];
    var EditablePrice = jsonObject.filter((product) => {               
        return product.is_editable_price  == false
    })

    EditablePrice.forEach((mainProduct) => {
        var productPriceTotal = mainProduct.products.reduce((sum,number) => {
            return sum+number.weight
      }, 0)

      array.push({name: mainProduct.name, totalSubProductWeight: productPriceTotal});
      
      })
      console.log(array);

    // console.log(EditablePrice);
}

function findFirsttypeNotNull(jsonObject)  {
    var i = 0;
    for (i = 0; i < jsonObject.length; i++) {
        const subProducts = jsonObject[i].products
        
        if (subProducts.length > 0) {
    
            var countSubProductTypeNotNull = subProducts.filter((product) => {  
                const hasProductType = product.hasOwnProperty('type')
                return hasProductType && product.type.length > 0 
            }).length
    
            if(countSubProductTypeNotNull > 0) {
                return i;
            }
        }
      }
}




function getTypeIdArrayWhichSubProductHasAllType(jsonObject)  {
    var typeIdArray = [];

    jsonObject.forEach((product) => {     
        const subProductList = product.products

        const hasTypeKeyAndHasType = subProductList.every((subProduct) => {
            const hasProductTypeKey = subProduct.hasOwnProperty('type')

            if (hasProductTypeKey) {
                const isProductTypeNotEmpty = subProduct.type.length > 0 
                return hasProductTypeKey && isProductTypeNotEmpty
            }

            return hasProductTypeKey
          })

          if (hasTypeKeyAndHasType) {
            subProductList.forEach((eachType) => {
                eachType.type.forEach((t) => {
                    typeIdArray.push(t.id)
                })
            })
          }
    })
    
    return typeIdArray;
}




    











